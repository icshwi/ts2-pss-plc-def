#!/usr/bin/env python

import os
import sys

wc = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.curdir)
sys.path.append(wc)
sys.path.append(os.path.abspath('/opt/plcfactory'))

from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()

git = "https://gitlab.esss.lu.se/icshwi/ts2-pss-plc-def"
#wc = None

# External links for TS2_ODH_MONITOR
factory.addLink("TS2_ODH_MONITOR", "BEAST TEMPLATE", git, wc)
factory.addLink("TS2_ODH_MONITOR", "EPI", git, wc)

# External links for TS2_PSS_AREA
factory.addLink("TS2_PSS_AREA", "BEAST TEMPLATE", git, wc)
factory.addLink("TS2_PSS_AREA", "EPI", git, wc)

#
# Adding IOC: KG-GTA:PSS-IOC-1
#
ioc = factory.addIOC("KG-GTA:PSS-IOC-1")
ioc.setProperty("Hostname", "pss-ts2-vm-ioc.tn.esss.lu.se")
ioc.setProperty("EPICSVersion", "7.0.6.1")
ioc.setProperty("E3RequireVersion", "4.0.0")
ioc.addLink("IOC_REPOSITORY", "https://gitlab.esss.lu.se/iocs/manual/pss/ts2pss_sc_ioc_001.git", download = False)
ioc.addLink("BEAST TREE[TS2_PSS]", git, wc)

#
# Adding PLC: KG-GTA:PSS-GPLC-1
#
plc = ioc.addPLC("KG-GTA:PSS-GPLC-1")
# Properties
plc.setProperty("EPICSModule", "['kg-gta_pss-gplc-1']")
plc.setProperty("EPICSSnippet", "['kg-gta_pss-gplc-1']")
plc.setProperty("PLCF#EPICSToPLCDataBlockName", "EPICSToPLC")
plc.setProperty("PLCF#PLCToEPICSDataBlockName", "PLCToEPICS")
plc.setProperty("PLCF#EPICSToPLCDataBlockStartOffset", "0")
plc.setProperty("PLCF#PLCToEPICSDataBlockStartOffset", "0")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBPort", "502")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBConnectionID", "255")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7ConnectionID", "256")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7Port", "2000")
plc.setProperty("PLCF#PLC-EPICS-COMMS:Endianness", "BigEndian")
plc.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "259")
plc.setProperty("PLC-EPICS-COMMS: GatewayDatablock", "dbSTD_to_Gateway.data")
plc.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "10")
plc.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "30")
plc.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "30")
plc.setProperty("Hostname", "pss-ts2-gateway-plc.tn.esss.lu.se")
plc.setProperty("PLCF#PLC-EPICS-COMMS: PLCPulse", "Pulse_100ms")
# External links
plc.addLink("EPI[TS2_PLC]", git, wc)
plc.addLink("BEAST TEMPLATE[TS2_PLC]", git, wc)

#
# Adding device KG-GTA:PSS-PLC-1 of type PSS_PROCESS_PLC
#
dev = plc.addDevice("PSS_PROCESS_PLC", "KG-GTA:PSS-PLC-1")
# External links
dev.addLink("BEAST TEMPLATE", git, wc)
dev.addLink("EPI", git, wc)

#
# Adding device KG-GTA:PSS-Area-TS2 of type TS2_PSS_AREA
#
dev1 = dev.addDevice("TS2_PSS_AREA", "KG-GTA:PSS-Area-TS2")

#
# Adding device TS2-010Row:CnPw-U-012 of type RACK
#
dev1 = dev.addDevice("RACK", "TS2-010Row:CnPw-U-012")
# External links
dev1.addLink("BEAST TEMPLATE[TS2_RACK]", git, wc)
dev1.addLink("EPI[TS2_RACK]", git, wc)


#
# Saving the created CCDB
#
factory.save("ts2-pss")
